# Sonómetro 

El sonómetro es un instrumento para medir niveles de presión sonora.

## Proyecto

Se presentan los detalles generales para la construcción de un sonómetro de bajo costo pero alta precisión tomando
como base microcontroladores o placas Arduino. Se presenta implementacoión usando un 68HC08 para aprovechar unos dispositivos que ya nadie usaba en el laboratorio.

## Funcionamiento del Dispositivo

El sonómetro toma una muestra del voltaje generado por el preamplificador de audio y lo convierte a su correspondiente valor digital por medio de un conversor análogo digital de 8 bits. De esto se desprende que el valor mínimo de voltaje que el instrumento puede discriminar es de 20mV y el valor máximo de 5V – 255 posibles valores de niveles de sonido medibles.

El filtro externo pasabajos se encarga de ecualizar la señal proveniente del preamplificador y darle la curva característica tipo A. En este punto se puede explorar nuevos filtros que brinden una respuesta más exacta a la curva tipo A. 

Cuando no se usa el filtro externo solo uno de los canales ADC del microcontrolador es usado.

La conversión a escala logarítmica es del tipo tabla exacta por lo cual se carga dicha tabla en el microcontrolador para realizar una relación uno a uno con los valores digitalizados. 

La tabla se formó a partir de la caracterización de la respuesta del sensor a valores de niveles de sonido conocidos y calibrados con el sonómetro del laboratorio de ingeniería. La ecuación empírica que rige el instrumento en modo promedio y fast es:

Y=18,256*LN(X)-34,691

La fidelidad que presenta este instrumento a aquel del laboratorio deriva de esta forma de elaboración de la tabla.

Una mejora rápida y poco costosa sería una nueva conformación de las tablas de traducción a partir de la calibración basada en un sonómetro de tipo digital.
