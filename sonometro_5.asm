*********************************************************************************************
*                       LABORATORIO: SONOMETRO
*
* Paulo Cesar Coronado
*
* OBJETIVO:
* Crear las rutinas de programacin para el sistema SOUDN
*********************************************************************************************



$INCLUDE 'jl3regs.inc'

PTD6    EQU     6
PTD7    EQU     7
RS   	EQU	4
E    	EQU	5
RAM  	EQU	$80H
ROM  	EQU	$EC00H
*Posicin de memoria para guardar las decenas
DECENA	EQU	80H
*Posicin de memoria para guardar las centenas
CENTENA	EQU	81H
NUMERO	EQU	82H
*Posicin de memoria para guardar las unidades
UNIDAD 	EQU	83H
CICLO   EQU     85H
PROMEDIO_2   EQU     86H
PROMEDIO     EQU     87H
TTG_MODO     EQU     89H

	ORG RAM


	ORG ROM


*****************INICIALIZAR PILA
	RSP
****************DESHABILITAR EL COP
EMPEZAR	BSET	0,CONFIG1

****************PUERTO B COMO SALIDA
	MOV	#0FFH,DDRB

****************************************
* PUERTO D COMO SALIDA EXCEPTO BITS 6 Y 7
* 0011 1111 (3FH)
****************************************
	MOV	#03FH,DDRD




*ADSCR: Registro de Control y Estado del conversor Anlogo/Digital
*******************************************************************
*   7     6    5   4   3   2   1   0
*  COCO AIEN ADC0 CH4 CH3 CH2 CH1 CH0
* Al cual se le carga: 0010 1001 (29H)
*
* ADCO: 1 Para que el sistema realice una conversin continua del
*         valor obtenido en el amplificador de audio
*
* Seleccionando el ADC9 que tiene su entrada en el PTD2 (Pin 17)
*******************************************************************

	MOV	#29H,ADSCR
**********************************************************
*Ms informacin en:
*http://mcu.motsps.com/
*Nombre del documento: 9JL3R1.PDF
**********************************************************

*******************************************************************
* RUTINAS DE INICIALIZACIN DE LA PANTALLA LCD
*******************************************************************

*INICIALIZAR PANTALLA LCD


        BCLR	E,PTD
       	BCLR    RS,PTD
	MOV	#38H,PTB
        BSET	E,PTD
        BSR     TIEMPO_1
	BCLR	E,PTD
        BSR     TIEMPO_1

*APAGAR PANTALLA

APAGAR  BCLR	E,PTD
       	BCLR    RS,PTD
	MOV	#08H,PTB
        BSET	E,PTD
        BSR     TIEMPO_1
	BCLR	E,PTD
        BSR     TIEMPO_1

*PRENDER PANTALLA

PRENDER BCLR	E,PTD
       	BCLR    RS,PTD
	MOV	#0CH,PTB
        BSET	E,PTD
        BSR     TIEMPO_1
	BCLR	E,PTD
        BSR     TIEMPO_1
        BRA     LIMPIAR
        
******************************************************
* TIEMPO NECESARIO PARA COLOCAR UN DATO EN EL LCD
******************************************************
TIEMPO_1        LDHX    #9000T
CICLO_1         AIX     #-1
                CPHX     #0
                BNE     CICLO_1
                RTS
******************************************************

*LIMPIAR PANTALLA

LIMPIAR BCLR	E,PTD
       	BCLR    RS,PTD
	MOV	#01H,PTB
        BSET	E,PTD
        BSR     TIEMPO_1
	BCLR	E,PTD
        BSR     TIEMPO_1
        BRA     MODO

**********************************************************
*Ms informacin en:
*http://monografias.com
*Nombre del Trabajo:Control de un mdulo LCD con PIC16F84a
**********************************************************

********************************************************
*Escribir en la pantalla LCD
*******************************************************
ESCRIBIR_0  BSR     TIEMPO_1
            BCLR	E,PTD
       	    BSET    RS,PTD
	    STA	PTB
            BSR     TIEMPO_1
            BSET	E,PTD
            BSR     TIEMPO_1
	    BCLR	E,PTD
            RTS
********************************************************
**********************************************************
* Comprobar el estado de los bits 6 y 7 del puerto D
**********************************************************
* PTD6   PTD7    ACCION
*  0      0      L50
*  0      1      FAST
*  1      0      FLAT
*  1      1      PROMEDIO
**********************************************************

MODO    LDA     PTD

        AND     #11000000Q
        CMP     #11000000Q
        BEQ     A_FAST
        CMP     #00H
        BEQ     A_PROM
        CMP     #10000000Q
        BEQ     A_FLAT
        BRA     MODO

A_FAST  JMP     FAST
A_PROM  JMP     PROM
A_FLAT  JMP     FLAT
********************************************************************************

*MOSTRAR FAST
*- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
*M O D O : F A S T
*********************************************************************************
FAST    LDA	#0
        STA     CICLO
        TAX
SALTO	LDA	DBA,X
        BSR     ESCRIBIR_0
        INC     CICLO
        LDA     CICLO
        TAX
        CMP     #8
        BNE     SALTO
        
        BSR     LLENAR
        LDA	#0
        STA     CICLO
        TAX

FAST_1	LDA	L_FAST,X
        BSR     ESCRIBIR_0
        INC     CICLO
        LDA     CICLO
        TAX
        CMP     #9
        BNE     FAST_1
        MOV     #3,TTG_MODO
        JMP     INICIO
********************************************************************************
* RELLENAR DE ESPACIOS EN BLANCO
*******************************************************

LLENAR  LDA	#0
        STA     CICLO
        TAX
LLEN_0  LDA     L_ESPACIO
        BSR     ESCRIBIR_0
        INC     CICLO
        LDA     CICLO
        TAX
        CMP     #26
        BNE     LLEN_0
        RTS

*********************************************************************************
*MOSTRAR PROMEDIO
*- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
*M O D O : P R O M E D I O
*********************************************************************************
PROM    LDA	#0
        STA     CICLO
        TAX
SALTOP	LDA	DBA,X
        BSR     ESCRIBIR_0
        INC     CICLO
        LDA     CICLO
        TAX
        CMP     #8
        BNE     SALTOP
                                                                                     
        BSR     LLENAR
        LDA	#0
        STA     CICLO
        TAX

PROM_1	LDA	L_PROM,X
        BSR     ESCRIBIR_1
        INC     CICLO
        LDA     CICLO
        TAX
        CMP     #9
        BNE     PROM_1
        MOV     #0,TTG_MODO
        JMP     INICIO
*********************************************************************************
*MOSTRAR FLAT
*- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
*M O D O : F L A T
*********************************************************************************
FLAT 	LDA	#0
        STA     CICLO
        TAX
SALTOF	LDA	L_DB,X
        BSR     ESCRIBIR_1
        INC     CICLO
        LDA     CICLO
        TAX
        CMP     #8
        BNE     SALTOF
                                                                                     
        BSR     LLENAR
        LDA	#0
        STA     CICLO
        TAX


FLAT_1	LDA	L_FLAT,X
        BSR     ESCRIBIR_1
        INC     CICLO
        LDA     CICLO
        TAX
        CMP     #9
        BNE     FLAT_1
        MOV     #2,TTG_MODO
        JMP     INICIO
********************************************************************************

********************************************************
*Escribir en la pantalla LCD
*******************************************************
ESCRIBIR_1  BSR     TIEMPO_4
            BCLR	E,PTD
       	    BSET    RS,PTD
	    STA	PTB
            BSR     TIEMPO_4
            BSET	E,PTD
            BSR     TIEMPO_4
	    BCLR	E,PTD
            RTS

*******************************************************
******************************************************
* TIEMPO NECESARIO PARA COLOCAR UN DATO EN EL LCD
******************************************************
TIEMPO_4        LDHX    #9000T
CICLO_4         AIX     #-1
                CPHX     #0
                BNE     CICLO_4
                RTS
******************************************************





INICIO  LDA     TTG_MODO

        CMP     #11Q
        BEQ     T_FAST
        CMP     #00Q
        BEQ     T_PROM
        CMP     #10Q
        BEQ     T_FLAT

T_FAST  JMP     TTG_FAST
T_PROM  JMP     TTG_PROM
T_FLAT  JMP     TTG_FLAT

*******************************************************
*Retardo entre medidas
*******************************************************

TIEMPO_3        
CICLO5          LDHX    #2000T
CICLO4          AIX     #-1
                CPHX    #0
                BNE     CICLO4
                RTS

********************************************************************************

TTG_FAST        LDA     ADR
                CLRH
                TAX
                LDA    TTG,X

                JMP     INICIAR                
********************************************************************************

TTG_PROM        MOV     #00H,PROMEDIO
                MOV     #00H,PROMEDIO_2

                 MOV     #100T,CICLO
PROMEDIAR        LDA     ADR
                 BSR     TIEMPO_3
                 ADD     PROMEDIO
                 BCS     LLEVA
                 BRA     GUARDAR
LLEVA            INC     PROMEDIO_2
GUARDAR          STA     PROMEDIO
                 DBNZ    CICLO,SEGUIR
                 BRA     DIVIDIR
SEGUIR           BRA     PROMEDIAR

DIVIDIR          LDHX   PROMEDIO_2
                 TXA
                 LDX    #100T
                 DIV
                 CLRH
                 TAX
                 LDA    TTG,X

                 JMP    INICIAR
                 
**********************************************************************************

TTG_FLAT        MOV     #00H,PROMEDIO
                MOV     #00H,PROMEDIO_2

                 MOV     #100T,CICLO
PROMEDIARF       LDA     ADR
                 BSR     TIEMPO_3
                 ADD     PROMEDIO
                 BCS     LLEVAF
                 BRA     GUARDARF
LLEVAF           INC     PROMEDIO_2
GUARDARF         STA     PROMEDIO
                 DBNZ    CICLO,SEGUIRF
                 BRA     DIVIDIRF
SEGUIRF          BRA     PROMEDIARF

DIVIDIRF         LDHX   PROMEDIO_2
                 TXA
                 LDX    #100T
                 DIV
                 CLRH
                 TAX
                 LDA    TTG_F,X

                 JMP    INICIAR
**********************************************************************************
******************************************************
* TIEMPO NECESARIO PARA COLOCAR UN DATO EN EL LCD
******************************************************
TIEMPO          LDHX    #9000T
CICLO1          AIX     #-1
                CPHX     #0
                BNE     CICLO1
                RTS
******************************************************

*MOVER EL CURSOR AL INICIO DE LA PANTALLA

INICIAR BCLR	E,PTD
       	BCLR    RS,PTD
	MOV	#02H,PTB
        BSET	E,PTD
        BSR     TIEMPO
	BCLR	E,PTD
        BSR     TIEMPO
            

                 

	LDX	#100T
	DIV
	STA	CENTENA
	STHX	NUMERO
	LDA	NUMERO
	CLRH
	LDX	#10T
	DIV
	STA	DECENA
	STHX	UNIDAD

*MOSTRAR CENTENA

CENT    CLRH
	LDX	CENTENA
	LDA	TABLA,X

        BSR     ESCRIBIR_3
        JMP     DECEN




*MOSTRAR DECENA

DECEN 	LDX	DECENA
	LDA	TABLA,X
        BSR     ESCRIBIR_3

********************************************************

*MOSTRAR UNIDAD

	LDX	UNIDAD
	LDA	TABLA,X

        BSR     ESCRIBIR_3




FINAL          JMP     INICIO


*******************************************************
*Retardo entre medidas
*******************************************************

TIEMPO_2        LDA     #3T
CICLO3          LDHX    #65000T
CICLO2          AIX     #-1
                CPHX    #0
                BNE     CICLO2
                DBNZA   CICLO3
                RTS
********************************************************
*Escribir en la pantalla LCD
*******************************************************
ESCRIBIR_3  BSR     TIEMPO3
            BCLR	E,PTD
       	    BSET    RS,PTD
	    STA	PTB
            BSR     TIEMPO3
            BSET	E,PTD
            BSR     TIEMPO3
	    BCLR	E,PTD
            RTS
********************************************************

******************************************************
* TIEMPO NECESARIO PARA COLOCAR UN DATO EN EL LCD
******************************************************
TIEMPO3         LDHX    #9000T
CICLO13          AIX     #-1
                CPHX     #0
                BNE     CICLO13
                RTS
******************************************************

TABLA	DB	30H,31H,32H,33H
        DB      34H,35H,36H,37H
        DB      38H,39H


TTG     DB	40T,45T,49T,51T,54T,55T,57T,59T,60T
        DB	61T,62T,63T,64T,65T,66T,66T,67T,68T
        DB	68T,69T,70T,70T,71T,71T,72T,72T,73T
        DB	73T,73T,74T,74T,75T,75T,75T,76T,76T
        DB	76T,77T,77T,77T,78T,78T,78T,79T,79T
        DB	79T,79T,80T,80T,80T,80T,81T,81T,81T
        DB	81T,81T,82T,82T,82T,82T,82T,83T,83T
        DB	83T,83T,83T,84T,84T,84T,84T,84T,84T
        DB	85T,85T,85T,85T,85T,85T,86T,86T,86T
        DB	86T,86T,86T,87T,87T,87T,87T,87T,87T
        DB	87T,87T,88T,88T,88T,88T,88T,88T,88T
        DB	88T,89T,89T,89T,89T,89T,89T,89T,89T
        DB	90T,90T,90T,90T,90T,90T,90T,90T,90T
        DB	91T,91T,91T,91T,91T,91T,91T,91T,91T
        DB	91T,92T,92T,92T,92T,92T,92T,92T,92T
        DB	92T,92T,92T,93T,93T,93T,93T,93T,93T
        DB	93T,93T,93T,93T,93T,93T,94T,94T,94T
        DB	94T,94T,94T,94T,94T,94T,94T,94T,94T
        DB	94T,95T,95T,95T,95T,95T,95T,95T,95T
        DB	95T,95T,95T,95T,95T,96T,96T,96T,96T
        DB	96T,96T,96T,96T,96T,96T,96T,96T,96T
        DB	96T,96T,97T,97T,97T,97T,97T,97T,97T
        DB	97T,97T,97T,97T,97T,97T,97T,97T,97T
        DB	97T,98T,98T,98T,98T,98T,98T,98T,98T
        DB	98T,98T,98T,98T,98T,98T,98T,98T,98T
        DB	99T,99T,99T,99T,99T,99T,99T,99T,99T
        DB	99T,99T,99T,99T,99T,99T,99T,99T,99T
        DB	99T,100T,100T,100T,100T,100T,100T,100T,100T

TTG_F   DB	26T,30T,36T,40T,43T,46T,48T,50T,52T
        DB	53T,55T,56T,57T,58T,59T,60T,61T,62T
        DB	62T,63T,64T,65T,65T,66T,66T,67T,68T
        DB	68T,69T,69T,70T,70T,70T,71T,71T,72T
        DB	72T,72T,73T,73T,74T,74T,74T,75T,75T
        DB	75T,76T,76T,76T,76T,77T,77T,77T,78T
        DB	78T,78T,78T,79T,79T,79T,79T,80T,80T
        DB	80T,80T,80T,81T,81T,81T,81T,81T,82T
        DB	82T,82T,82T,82T,83T,83T,83T,83T,83T
        DB	84T,84T,84T,84T,84T,84T,85T,85T,85T
        DB	85T,85T,85T,86T,86T,86T,86T,86T,86T
        DB	86T,87T,87T,87T,87T,87T,87T,87T,88T
        DB	88T,88T,88T,88T,88T,88T,88T,89T,89T
        DB	89T,89T,89T,89T,89T,89T,90T,90T,90T
        DB	90T,90T,90T,90T,90T,90T,91T,91T,91T
        DB	91T,91T,91T,91T,91T,91T,91T,92T,92T
        DB	92T,92T,92T,92T,92T,92T,92T,92T,93T
        DB	93T,93T,93T,93T,93T,93T,93T,93T,93T
        DB	93T,94T,94T,94T,94T,94T,94T,94T,94T
        DB	94T,94T,94T,95T,95T,95T,95T,95T,95T
        DB	95T,95T,95T,95T,95T,95T,95T,96T,96T
        DB	96T,96T,96T,96T,96T,96T,96T,96T,96T
        DB	96T,96T,97T,97T,97T,97T,97T,97T,97T
        DB	97T,97T,97T,97T,97T,97T,97T,97T,98T
        DB	98T,98T,98T,98T,98T,98T,98T,98T,98T
        DB	98T,98T,98T,98T,98T,99T,99T,99T,99T
        DB	99T,99T,99T,99T,99T,99T,99T,99T,99T
        DB	99T,99T,99T,99T,100T,100T,100T,100T,100T
    

DBA     DB      32T,32T,32T,91T,64H,42H,41H,93T

L_DB    DB      32T,32T,32T,91T,64H,42H,93T,20H

L_FAST  DB      77T,79T,68T,79T,58T,70T,65T,83T,84T

L_PROM  DB      77T,79T,68T,79T,58T,80T,82T,79T,77T

L_FLAT  DB      77T,79T,68T,79T,58T,70T,76T,65T,84T



L_ESPACIO DB    32T

        ORG     0FFFEH
        DW      EMPEZAR



